#include <stdio.h>

int main(void)
{
	int num;

	//get number from keyboard
	printf("Enter number: ");
	scanf("%d", &num);

	//check number
	if(num == 0) 
	{
		printf("Zero");
	}
	else if(num < 0)
	{
		printf("Negative");
	}
	else if(num > 0)
	{
		printf("Positive");
	}

	return 0;
}
